//
//  ViewController.swift
//  mapsApp
//
//  Created by Juan Erazo on 7/20/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, GMSMapViewDelegate {
    
    var locationManager = CLLocationManager()
    var mapView:GMSMapView!
    @IBOutlet weak var gooogleMapsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        addMap()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func addMap() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        //mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView = GMSMapView.map(withFrame: gooogleMapsView.bounds, camera: camera)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        //view = mapView
        gooogleMapsView.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        /*let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView*/
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        locationManager.requestWhenInUseAuthorization()
        let location = locationManager.location?.coordinate
        let camera = GMSCameraPosition.camera(withLatitude: (location?.latitude)!, longitude: (location?.longitude)!, zoom: 15.0)
        mapView.animate(to: camera)
        
        return true
    }
    
    @IBAction func hibridoButtonPressed(_ sender: Any) {
        mapView.mapType = .hybrid
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let marker = GMSMarker(position:position)
        
        let destination = CLLocation(latitude:coordinate.latitude, longitude:coordinate.longitude)
        let locValue = locationManager.location!
        let distance = destination.distance(from: locValue)
            
        marker.title = "Mi marcador"
        marker.snippet = String(distance)
        marker.map = mapView
        
        
    }
    
    
}

